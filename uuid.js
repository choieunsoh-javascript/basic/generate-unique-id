const id1 = () => {
  // * Math.random should be unique because of its seeding algorithm.
  // * Convert it to base36 (numbers + letters) and grab the first 9 characters
  // * after the decimal.
  return Math.random().toString(36).substr(2, 9);
};

const id2 = () => {
  // * Math.random should be unique because of its seeding algorithm.
  // * Convert it to base16 (numbers + letters) and grab the first 9 characters
  // * after the decimal.
  return Math.random().toString(16).substr(2, 9);
};

const UUID = () => {
  let dt = new Date().getTime();
  let uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx";
  return uuid.replace(/[xy]/g, (c) => {
    const r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    return (c === "x" ? r : (r & 0x3) | 0x8).toString(16);
  });
};

console.log(id1());
console.log(id1());
console.log(id2());
console.log(id2());
console.log(UUID());
console.log(UUID());
